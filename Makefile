.PHONY: help

STACK=docker-compose.yml

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

all: build gitlab docker 

build: ## local docker build
	docker build -t firefox-headless:58 .

gitlab: ## build for registry.gitlab.com
	make build
	docker tag firefox-headless:58 registry.gitlab.com/markuman/firefox-headless:58
	
docker: ## build for dockerhub
	make build
	docker tag firefox-headless:58 markuman/firefox-headless:58
	
screenshot: ## make a screenshot of https://developer.mozilla.com and save it to /tmp of host system
	docker run -ti -v /tmp:/tmp firefox-headless:58 firefox -headless -screenshot https://developer.mozilla.com
	
