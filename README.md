# firefox-headless

Includes also nodejs for using this image with karma.  
Image size ~110MB (chrome headless puppeteer image has a size above 500MB!)

# demo

```
make build
make screenshot
```

Take a look at `/tmp/screenshot.png`

# git mirrors

* https://gitlab.com/markuman/firefox-headless
* https://github.com/markuman/firefox-headless

# docker registries

* `docker pull registry.gitlab.com/markuman/firefox-headless:58`
* `docker pull markuman/firefox-headless:58`
